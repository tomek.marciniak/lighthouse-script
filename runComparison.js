const checkUrl = require('./checkUrl');
const mapResults = require('./mapResults');

const runComparison = (urls, attempts) => {
  let results = {};
  
  for (let i = 0; i < attempts; i++) {
    urls.forEach(url => {
      process.stdout.write(`\rChecking ${url}, #${i + 1}/${attempts}                    `);
      const result = checkUrl(url);

      if (result) {
        if (!results[url]) results[url] = [];
        results[url].push(result);
      }
    });
  }

  return mapResults(results);
};

module.exports = runComparison;
