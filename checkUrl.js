const spawnSync = require('child_process').spawnSync;
const lighthouseCli = require.resolve('lighthouse/lighthouse-cli');

const checkUrl = url => {
  const { status = -1, stdout } = spawnSync('node', [
    lighthouseCli,
    url,
    '--output=json',
    '--only-categories=performance',
    '--chrome-flags="--headless"'     
  ], { maxBuffer: 1024 * 1024 * 1024 });
  if (!!status) {
    process.stdout.write('\rLighthouse failed, retrying run...');
    return checkUrl(url);
  }
  return JSON.parse(stdout.toString());
}

module.exports = checkUrl;