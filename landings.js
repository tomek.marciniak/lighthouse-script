const runComparison = require('./runComparison');
const printScores = require('./printScores');

const ATTEMPTS = 20;
let DESCRIPTION;
// const DESCRIPTION = 'async chunks, all priority Image, minChunkSize 20kb';
const PAGE = 'photo-editor';

const hosts = [
  // 'https://picsart.com',
  // 'https://feature-pia-190113-lazy-loading-app-shell.picsartstage2.com',
  // 'https://picsart.com',
  'https://picsartstage2.com',
  'https://feature-pia-190122-baseline-app-shell.picsartstage2.com',
  'https://feature-pia-190122-tweaks-2-app-shell.picsartstage2.com'
];

if (DESCRIPTION) {
  console.log('-----------------------------------------------------------');
  console.log(DESCRIPTION);
}

console.log('-----------------------------------------------------------');
console.log(`PAGE: ${PAGE} #${ATTEMPTS} attempts`);

const urls = hosts.map(host => PAGE ? `${host}/${PAGE}` : host);
const results = runComparison(urls, ATTEMPTS);

printScores(results);
