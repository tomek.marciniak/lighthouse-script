const {
  computeMedianRun,
} = require('lighthouse/lighthouse-core/lib/median-run.js');
const path = require('path');

const mapResults = results => {
  const medianResults = {};

  for (let [key, value] of Object.entries(results)) {
    medianResults[key] = computeMedianRun(value);
  }
  return medianResults;
}

module.exports = mapResults;
