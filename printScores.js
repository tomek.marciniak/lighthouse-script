
const printScoresForUrl = (url, results) => {
  console.log('-----------------------------------------------------------');
  console.log(`${url} scores `);
  console.log(`Performance: ${results.categories.performance.score * 100}`);
  // console.log(`Accessibility: ${results.categories.accessibility.score * 100}`);
  console.log(`FCP: ${results.audits['first-contentful-paint'].displayValue}`);
  console.log(
    `LCP: ${results.audits['largest-contentful-paint'].displayValue}`
  );
  console.log(`Speed Index: ${results.audits['speed-index'].displayValue}`);
  console.log(
    `Total Blocking Time: ${results.audits['total-blocking-time'].displayValue}`
  );
  console.log('-----------------------------------------------------------');
}

const printScores = results => {
  for (let [url, res] of Object.entries(results)) {
    printScoresForUrl(url, res);
  }
}

module.exports = printScores;
